var myChart;

function addDataSet(delays, name) {
    let color = "rgba(24, 188, 156, 1)";

    let Dataset = {
        label: name,
        backgroundColor: color,
        borderColor: color,
        data: delays,
        fill: false
    };

    chartConfig.data.datasets.push(Dataset);
    window.myChart.update();
}

function initChart(config) {
    const ctx = document.getElementById('myChart').getContext('2d');
    myChart = new Chart(ctx, config);
}

let chartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: []
    },
    options: {
        title: {
            display: true
        }
    }
};

function statsDataLoader() {
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://rnv.zap60768-1.plesk05.zap-webspace.com/public/getGeneralStats.php",
        "method": "GET"
    };

    $.ajax(settings)
        .done(function(response) {
            //Get Information
            response = JSON.parse(response);
            let dataDelays = [];
            let labels = [];
            for (let i = 0; i < response.length; i++) {
                dataDelays.push(response[i].delays);
                labels.push(response[i].created);
            }

            chartConfig.data.labels = labels;

            initChart(chartConfig);

            addDataSet(dataDelays, "Verspätung in Minuten");
        });
}