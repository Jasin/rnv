
# rnv-monitor.de - Alle Verspätungen auf einen Blick
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://rnv-monitor.de">
    <img src="https://rnv-monitor.de/screenshot.png" alt="Logo" width="1280" height="640">
  </a>

  <p align="center">
	  An open source project to track public transport
    <br />
    <a href=""><strong>Explore the docs » [TO DO]</strong></a>
    <br />
    <br />
    <a href="https://rnv-monitor.de">View Live</a>
    ·
    <a href="https://gitlab.com/Jasin/rnv/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Jasin/rnv-server">Related server</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## 📋 Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
* [Authors](#authors)
* [Acknowledgements](#acknowledgements)
* [Docs](#docs)



<!-- ABOUT THE PROJECT -->
## 📰 About The Project

My idea was to track all public transport delays in the Rhein-Necker region over a period of one year (as well as possible).

If you want to know more, checkout[this](https://rnv-mpnitor.de#idea). <br>

<!-- Getting Started -->
## 🏁 Getting Started

You can just clone the project.
You also need the [backend](https://gitlab.com/Jasin/rnv-server)
Change the URLs of the requests to the location of your backend. 
Maybe I'll make it easier some day, sorry

<!-- CONTACT -->
## ✍️ Authors

[Jasin Goldkorn (WORK IN PROGRESS)](https://anira.tech/jasin-goldkorn) - jasingoldkorn.1@gmx.de 


<!-- ACKNOWLEDGEMENTS -->
## 💭 Acknowledgements
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Start Bootstrap Theme](https://startbootstrap.com/themes/sb-admin-2/)
* [Font Awesome](https://fontawesome.com)
* [RNV API](https://opendata.rnv-online.de/startinfo-api)

<!-- ACKNOWLEDGEMENTS -->
## 📚 Docs

TODO

<!-- ACKNOWLEDGEMENTS -->
### ✨Special thanks to

[@diverent2](https://github.com/diverent2)
[@arinaainfarbe](https://www.instagram.com/arinainfarbe/?igshid=1vur63e8gc793)