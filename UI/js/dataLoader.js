let settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://rnv.zap60768-1.plesk05.zap-webspace.com/public/getInfos.php",
    "method": "GET"
};

dataLoader();

function dataLoader() {
    $.ajax(settings)
        .done(function(response) {
            response = JSON.parse(response);

            let generelDelay = response.generalDelays;

            generelDelay = Number.parseFloat(generelDelay).toFixed(2);
            $('#mainTimeContent').html(generelDelay);

            let lastDelays = response.lastDelays;
            for (let i = 0; i < lastDelays.length; i++) {
                document.getElementById("row" + i + "number1").innerHTML = lastDelays[i].lineLabel; //Linie
                document.getElementById("row" + i + "number2").innerHTML = lastDelays[i].direction; //Richtung
                let abfahrtzeit = lastDelays[i].specID.split('_');
                abfahrtzeit = abfahrtzeit[1];
                document.getElementById("row" + i + "number3").innerHTML = abfahrtzeit; //Abfahrtzeit
                document.getElementById("row" + i + "number4").innerHTML = Number.parseFloat(lastDelays[i].delay).toFixed(2) + " Min"; //Verspätung
                document.getElementById("row" + i + "number5").innerHTML = lastDelays[i].created; //Timestamp
                if (lastDelays[i].transportation === "KOM") {
                    document.getElementById("row" + i + "number6").innerHTML = "Bus";
                } else if (lastDelays[i].transportation === "STRAB") {
                    document.getElementById("row" + i + "number6").innerHTML = "Straßenbahn";
                } else {
                    document.getElementById("row" + i + "number6").innerHTML = "Sonstige";
                }
            }
        });
}