<?php

$linesAndStops = getLinesAndStops();
$allStations = array();

foreach ($linesAndStops as $lineAndStop)
{
    $stops = $lineAndStop['stops'];
    $lineLabel = $lineAndStop['lineLabel'];

    echo("lineLabel: " . $lineLabel . "\n");

    $sumDelayForLineLabel = getSumDelaysForLineLabel($lineLabel);

    echo("Sumdelay " . $sumDelayForLineLabel . "\n");

    if($sumDelayForLineLabel != NULL)
    {
        foreach ($stops as $stop)
        {
            if(array_key_exists($stop['hafasID'], $allStations))
            {
                array_push($allStations[$stop['hafasID']]['delay'], $sumDelayForLineLabel);
            }
            else
            {
                $allStations[$stop['hafasID']] = array("hafasID" => $stop['hafasID'], "name" => $stop['name'], "lang" => $stop['longitude'], "lat" => $stop['latitude'], "delay" => [$sumDelayForLineLabel]);
            }
        }
    }
}

echo(json_encode($allStations));
die();

function getSumDelaysForLineLabel($lineLabel)
{
    $conn = new mysqli("localhost", "root", "");
    if ($conn->connect_error)
    {
        die("Couldn't build a connection to SQL-Server: " . $conn->connect_error);
    }

    $sql = "SHOW DATABASES LIKE 'rnv'";
    if (!mysqli_num_rows($conn->query($sql)) == 1)
    {
        echo("DB does not exists" . $conn->error);
    }
    $conn->select_db("rnv");
    $names = mysqli_query($conn, "SET NAMES 'utf8'");

    //sql
    $sql  = 'SELECT SUM(delay) as "sumDelay" FROM `tours` where lineLabel = "' . $lineLabel . '"';

    $res = mysqli_query($conn, $sql);
    if ($conn->errno)
    {
        die ("Fehler beim lesen der Datenbank");
    }

    $temp = array();

    while($row = $res->fetch_assoc())
    {
        $temp = $row;
    }

    return $temp['sumDelay'];
}

function getLinesAndStops()
{
    echo("Getting lines and stops \n");

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_PORT => "8080",
        CURLOPT_URL => "http://rnv.the-agent-factory.de:8080/easygo2/api/regions/rnv/modules/lines/allJourney",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: fe93ea0d-557d-df5a-9910-b306c9b9528b",
            "rnv_api_token: drmatglaf0pq7ku5gvbfpg5mbd"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err)
    {
        die("cURL Error #:" . $err);
    }

    $response = json_decode($response);
    $data = array();
    foreach ($response as $line)
    {
        $lineArray = array();
        foreach ($line->lineIDs as $stop)
        {
            $conn = new mysqli("localhost", "root", "");
            if ($conn->connect_error)
            {
                die("Couldn't build a connection to SQL-Server: " . $conn->connect_error);
            }

            $sql = "SHOW DATABASES LIKE 'rnv'";
            if (!mysqli_num_rows($conn->query($sql)) == 1)
            {
                echo("DB does not exists" . $conn->error);
            }
            $conn->select_db("rnv");
            $names = mysqli_query($conn, "SET NAMES 'utf8'");

            //sql
            $sql  = 'SELECT * FROM `stops` where hafasID = "' . $stop . '"';

            $res = mysqli_query($conn, $sql);
            if ($conn->errno)
            {
                die ("Fehler beim lesen der Datenbank");
            }

            $temp = array();

            while($row = $res->fetch_assoc())
            {
                array_push($temp, $row);
            }

            if(isset($temp[0]))
                array_push($lineArray, $temp[0]);
        }

        array_push($data, array("lineLabel" => $line->lineId, "stops" => $lineArray));
    }

    return $data;
}