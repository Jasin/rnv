<!-- Modal -->
<div class="modal fade" id="AddLine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Linie hinzufügen</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Linienname</label>
                    <small id="emailHelp" class="form-text text-muted">Beispielsweise: 4A oder 34</small>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="4A">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                <button type="button" id="addBtn" class="btn btn-primary" data-dismiss="modal">Hinzufügen</button>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function()
    {
        $("#addBtn").click(function ()
        {
            let inputLineLabel = $("#exampleInputEmail1").val();
            $("#exampleInputEmail1").val(""); //Clear the inputfield
            dataLoader(inputLineLabel, false);
        });
    });
</script>