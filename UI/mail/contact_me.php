<?php


if(!isset($_GET['name'])) 
{
    returnJson("Missing name", 50, $_GET);
}
if(!isset($_GET['email']))
{
  returnJson("Missing email", 50, $_GET);
}
if(!isset($_GET['message']))
{
  returnJson("Missing message", 50, $_GET);
}
if (!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) 
{
  returnJson("Email Invalid", 50, $_GET);
}

$APIKEY_PUBLIC = "b3a10ef8b9840816b298a6482360f731";
$APIKEY_PRIVATE = "4c4eb91b230d1c2a41d1be653ccd68ae";
require '../../vendor/autoload.php';
use \Mailjet\Resources;
$mj = new \Mailjet\Client($APIKEY_PUBLIC, $APIKEY_PRIVATE, true,['version' => 'v3.1']);

$body = [
    'Messages' => [
        [
            'From' => [
                'Email' => "bot@anira.tech",
                'Name' => "RNV Monitor"
            ],
            'To' => 
            [
                [
                    'Email' => "jasingoldkorn.1@gmx.de",
                    'Name' => "RNV Developer"
                ]
            ],
            'Subject' => "Mail from RNV Monitor",
            'TextPart' => "Name: " . $_GET['name'] . "; Email: " . $_GET['email'] . "; Message: " . $_GET['message'] . ";",
            'HTMLPart' => "Name: " . $_GET['name'] . "; Email: " . $_GET['email'] . "; Message: " . $_GET['message'] . ";"
        ]
    ]
];

$response = $mj->post(Resources::$Email, ['body' => $body]);

if($response->success())
{
    returnJson("Success", 20, $response->getData());
}
else
{
  returnJson("Error", 50, $response->getData());
}

function returnJson($message, $status, $data)
{
	$arr = array("status" => $status, "message" => $message, "data" => $data);

	die(json_encode($arr));
}

