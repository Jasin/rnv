<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rnv-Monitor Statistik</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-color: #18bc9c;
        }

        .form-control {
            width: 100%;
        }

        .wrapper {
            background: white;
            height: 90%;
            margin: 2%;
            padding: 2%;
        }

        #methodInfo {
            font-family: 'Lato', sans-serif;
            color: grey;
        }

        .button {
            width: 100%;
            margin-top: 20px;
            margin-bottom: 2%;
            background-color: #18bc9c;
            color: white;
            border: 3px solid #18bc9c;
            border-radius: 25px;
            -webkit-transition: 0.3s ease-in-out !important;
            transition: 0.3s ease-in-out !important;
        }

        .button:hover {
            background-color: white;
            color: #18bc9c;
            -webkit-transition: 0.3s ease-in-out !important;
            transition: 0.3s ease-in-out !important;
        }

        h4 {
            margin-top: 4%;
            width: 100%;
            text-align: center;
            color: #18bc9c;
        }

        .h4Underline {
            width: 10%;
            background-color: #18bc9c;
            height: 3px;
            margin-top: 5px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container">
            <div class="row"> 
                <div class="col-xs-2 col-md-2">
                    <a href="../../form"><button class="btn button"><i class="fas fa-arrow-left"></i></button></a>
                </div>
                <div class="col-md-10 col-xs-10">
                    <button class="btn button" data-toggle="modal" data-target="#AddLine"><i class="fas fa-plus-circle"></i> Daten hinzufügen</button>
                </div> 
            </div>
        </div>
        <div class="container"> <canvas class="mainChart" id="myChart"></canvas>
            <div class="row"> <small id="methodInfo"></small> </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script>
        var myChart;

        function generateColorAsString() 
        {
            /*Found: https://stackoverflow.com/questions/23095637/how-do-you-get-random-rgb-in-javascript*/
            let o = Math.round,
                r = Math.random,
                s = 255;
            return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ', 0.7)';
        }

        function addDataSet(delays, name) {
            let color = generateColorAsString();
            let Dataset = {
                label: name,
                backgroundColor: color,
                borderColor: color,
                data: delays,
                fill: false
            };
            chartConfig.data.datasets.push(Dataset);
            window.myChart.update();
        }

        function initChart(config) {
            const ctx = document.getElementById('myChart').getContext('2d');
            myChart = new Chart(ctx, config);
        }
        let chartConfig = {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                title: {
                    display: true
                }
            }
        };

        function dataLoader(lineLabel, first) 
        {
            let endDate = findGetParameter("endDate");
            let startDate = findGetParameter("startDate");
            let settings = 
                {
                    "async": true,
                    "crossDomain": true,
                    "url": "https://rnv.zap60768-1.plesk05.zap-webspace.com/public/getInfosForStatsByTime.php?lineLabel=" + lineLabel + "&endDate=" + endDate + "&startDate=" + startDate,
                    "method": "GET"
                };

            if (first === true) 
            {
                $.ajax(settings).done(function(response) 
                {
                    response = JSON.parse(response);
                    let dataDelays = [];
                    for (let i = 0; i < response.length; i++) {
                        dataDelays.push(response[i].delays);
                    }
                    let labels = [];
                    for (let e = 0; e < response.length; e++) {
                        labels.push(response[e].created);
                    }
                    chartConfig.data.labels = labels;
                    initChart(chartConfig);
                    addDataSet(dataDelays, lineLabel);
                });
            } else 
            {
                $.ajax(settings).done(function(response) 
                {
                    response = JSON.parse(response);
                    let dataDelays = [];
                    for (let i = 0; i < response.length; i++)
                    {
                        dataDelays.push(response[i].delays);
                    }
                    addDataSet(dataDelays, lineLabel);
                });
            }
        }

        function findGetParameter(parameterName) {
            var result = null,
                tmp = [];
            location.search.substr(1).split("&").forEach(function(item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
            return result;
        }
    </script>
    <script>
        $(document).ready(function()
         {
            let methodInfoLabel = document.getElementById("methodInfo");
            let method = findGetParameter("method");
            switch (method) 
            {
                    case "LineByTime":
                        methodInfoLabel.textContent = "Methode: Verspätung nach Linien sortiert. Alle erfassten Verspätungen pro Tag aufsummiert";
                        break;
                    default:
                        methodInfoLabel.textContent = "Methode: Nicht erfasst.";
            }

            dataLoader(findGetParameter("lineLabel"), true);
        });
    </script>
    <?php require_once("../AddLineModal.php"); ?>
</body>

</html>