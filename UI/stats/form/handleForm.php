<?php

$generalStats = getGeneralStatsInformation();
header("Location: ../View/index.php/?method=" . $_GET['Method'] . "&lineLabel=" .  $_GET['LineLabel'] . "&endDate=" . $_GET['endDate'] . "&startDate=" . $_GET['startDate']);

function getGeneralStatsInformation()
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://rnv.zap60768-1.plesk05.zap-webspace.com/public/getInfosForStatsByTime.php?lineLabel=" . $_GET['LineLabel'] . "&endDate=" . $_GET['endDate'] . "&startDate=" . $_GET['startDate'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);

    $err = curl_error($curl);

    curl_close($curl);

    if ($err)
    {
        header("Location: 404");
        exit;
    }
    else
    {
        $jsonResponse = json_decode($response);

        if ($jsonResponse === null)
        {
            header("Location: 404");
            exit;
        }
        else if(count($jsonResponse) == 0)
        {
            header("Location: 404");
            exit;
        }
        else
        {
            return strval($response);
            //header("Location: ../main?data=" . $response ."&lineLabel=" . $_GET['LineLabel'] . "&amount=" . $_GET['Time'] . "&method=" . $_GET['Method']);
        }
    }
}
